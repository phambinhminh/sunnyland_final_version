﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour {
    public GameMaster gm;
    public AudioSource sound;
    public Animator anim;
    public float animTime;
    public Health heal;

    public bool gem;
    public bool cherry;

    // Use this for initialization
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gem)
        {
            if (collision.CompareTag("Player"))
            {
                sound.Play();
                DestroyItem(animTime);
            }
        }
        if (cherry) {
            if (collision.CompareTag("Player"))
            {
                heal.health++;
                sound.Play();
                DestroyItem(animTime);
            }

        }
    }

    public void DestroyItem(float animTime) {
        StartCoroutine(Item_play(animTime));
    }

    IEnumerator Item_play(float animTime) {
        anim.SetTrigger("Collect");
        yield return new WaitForSeconds(animTime);
        Destroy(this.gameObject);
    }
}
