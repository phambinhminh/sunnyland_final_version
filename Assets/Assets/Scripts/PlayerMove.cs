﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
    public CharacterControllers controller;
    public Animator anim;
    public float runSpeed = 40f;
    public Collider2D m_collider;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;


    // Update is called once per frame
    void Update()
    {    
        PlayerWalkKeyBoard();
    }
    
    void PlayerWalkKeyBoard()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        anim.SetFloat("Speed", Mathf.Abs(horizontalMove));
        if (Input.GetButtonDown("Jump"))
        {
            anim.SetBool("IsJumping", true);
            jump = true;
        }

        if (Input.GetButtonDown("Crouch"))
        {
            crouch = true;
        }
        else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }
    }

    public void Onlanding()
    {
        anim.SetBool("IsJumping", false);
    }

    public void OnCrouch(bool isCrouching)
    {
        anim.SetBool("IsCrouching", isCrouching);
    }

    public void _TriggerHurt(float hurtTime) {
        StartCoroutine(HurtBlink(hurtTime));
    }

    IEnumerator HurtBlink(float hurtTime) {
        int enemyLayer = LayerMask.NameToLayer("Enemy");
        int playerLayer = LayerMask.NameToLayer("Player");
        Physics2D.IgnoreLayerCollision(enemyLayer, playerLayer);

        foreach (Collider2D col in controller.mycols)
        {
            m_collider.enabled = false;
            m_collider.enabled = true;
        }
        anim.SetLayerWeight(1, 1);
        yield return new WaitForSeconds(hurtTime);

        Physics2D.IgnoreLayerCollision(enemyLayer, playerLayer, false);
        anim.SetLayerWeight(1, 0);
    }

    void FixedUpdate()
    {

        // Move our character
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }
}

