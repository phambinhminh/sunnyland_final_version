﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu_Script : MonoBehaviour {
    public static bool GameIsPause = false;
    public GameObject pauseUI;

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (GameIsPause)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
	}

    public void Resume() {
        pauseUI.SetActive(false);
        GameIsPause = false;
        Time.timeScale = 1f;
    }

    void Pause() {
        pauseUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPause = true;
    }

    public void LoadMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu_Screen", LoadSceneMode.Single);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
