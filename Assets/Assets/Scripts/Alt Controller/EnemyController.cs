﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public float speed;
    public Transform ditection;
    public float distance_1;
    public float distance_2;
    public Animator anim;
    public LayerMask whatIsBlocking;
    public bool movingLeft = true;
    public AudioSource dead;

    [Header("Enemy Type")]
    [Space]
    public bool oposeum = false;
    public bool eagle = false;
    public bool frog = false;

    [Header("Eagle Control")]
    [Space]
    public bool eagle_vetical;
    public bool eagle_horizontal;
    private bool isUp;

    [Header("Frog Control")]
    [Space]
    public float frogJumpForce;
    public Rigidbody2D enemy_rb;
    public Transform enemy_feetPos;
    public float checkRaidus;
    public LayerMask whatIsGround;
    public bool isGrounded;

    private void Awake()
    {
        enemy_rb = GetComponent<Rigidbody2D>();
    }

    void _Move()
    {
        if (oposeum == true)
        {
            transform.Translate(Vector2.left * Time.fixedDeltaTime * speed * Time.timeScale);
        }

        if (eagle == true)
        {
            Transform current_pos = GetComponent<Transform>();
            float minY = -1f;
            float maxY = 1f;

            if (eagle_horizontal) {
                transform.Translate(Vector2.left * Time.fixedDeltaTime * speed * Time.timeScale);
            }
            if (eagle_vetical) {
                if (minY < current_pos.localPosition.y && current_pos.localPosition.y < maxY)
                {
                    if (maxY - current_pos.localPosition.y >= 0 && isUp == true)
                    {
                        transform.Translate(Vector2.up * Time.fixedDeltaTime * speed);
                    }
                    else if (maxY - current_pos.localPosition.y >= 0 && isUp == false)
                    {
                        transform.Translate(-Vector2.up * Time.fixedDeltaTime * speed);
                    }
                }
                else
                {
                    if (maxY - current_pos.localPosition.y <= 0)
                    {
                        isUp = false;
                        transform.Translate(-Vector2.up * Time.fixedDeltaTime * speed);
                    }
                    else if (minY - current_pos.localPosition.y > 0)
                    {
                        isUp = true;
                        transform.Translate(Vector2.up * Time.fixedDeltaTime * speed);
                    }
                }
            }
        }

        if (frog == true)
        {
            isGrounded = Physics2D.OverlapCircle(enemy_feetPos.position, checkRaidus, whatIsBlocking);
            if (isGrounded)
            {
                //transform.Translate(Vector2.left * Time.deltaTime * speed);
                enemy_rb.AddForce(((Vector2.up * 2 + Vector2.left) / 3) * frogJumpForce);// (new Vector2(0f, frogJumpForce ));
            }
        }
    }

    private void Update()
    {
        _Move();
        _ChangeSide();
    }

    public void _ChangeSide()
    {
        
        RaycastHit2D ground_hitInfo = Physics2D.Raycast(ditection.position, Vector2.down, distance_1, whatIsBlocking);
        Debug.DrawLine(ditection.position, new Vector3(ditection.position.x, ditection.position.y - 10, ditection.position.z), Color.blue);
        RaycastHit2D obstacle_hitInfo = Physics2D.Raycast(ditection.position, Vector2.left, distance_2,whatIsBlocking);
        Debug.DrawLine(ditection.position, new Vector3(ditection.position.x - 1, ditection.position.y, ditection.position.z), Color.blue);
        
        if (ground_hitInfo.collider == false)
        {
            if (movingLeft == true)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                movingLeft = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingLeft = true;
            }
            
        }

        if (obstacle_hitInfo.collider == true)
        {
            if (movingLeft == true)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                movingLeft = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingLeft = true;
            }

        }

    }

    public void _Hurt(float time) {
        StartCoroutine(DeadTime(time));
    }

    IEnumerator DeadTime(float time) {
        dead.Play();
        anim.SetTrigger("Dead");

        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
    }
}
