﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour {
    private Transform Player;
    public float minX, maxX, minY, maxY;
    // Use this for initialization
    void Start()
    {
        Player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Player != null)
        {
            Vector3 temp = transform.position;
            temp.x = Player.position.x;
            if (temp.x < minX)
            {
                temp.x = minX;
            }
            if (temp.x > maxX)
            {
                temp.x = maxX;
            }
            temp.y = Player.position.y;
            if (temp.y < minY)
            {
                temp.y = minY;
            }
            if (temp.y > maxY)
            {
                temp.y = maxY;
            }
            transform.position = temp;

        }

    }
}
