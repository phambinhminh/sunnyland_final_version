﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour {
    public int gem;
    public Text gemText;
    public int cherry;
    public Text cherryText;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        AddGem();
        AddCherry();
		
	}
    void AddGem()
    {
        gemText.text = (gem + "");
    }
    void AddCherry()
    {
        cherryText.text = ("" + cherry);
    }
}
